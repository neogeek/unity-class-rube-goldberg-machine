﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour {

	public Transform mainCamera;

	public GameObject startItem;

	public GameObject currentActiveItem;

	public GameObject[] items;

	void Start () {

		currentActiveItem = startItem;

		currentActiveItem.GetComponent<ItemController>().hasBeenActive = true;

		Invoke("KickOffEvent", 1);

	}

	void Update () {

		mainCamera.position = new Vector3(mainCamera.position.x, currentActiveItem.transform.position.y, mainCamera.position.z);

	}

	public void ChangeActiveItem (GameObject newObject) {

		 currentActiveItem = newObject;

	}

	public void KickOffEvent () {

		Vector3 maxBounds = startItem.GetComponent<Renderer>().bounds.max;
		Vector3 extendsBounds = startItem.GetComponent<Renderer>().bounds.extents;

		startItem.GetComponent<Rigidbody>().AddForceAtPosition(new Vector3(100.0f, 0, 0), new Vector3(maxBounds.x + extendsBounds.x, maxBounds.y, maxBounds.z), ForceMode.Force);

	}

	public void ResetEvent () {

		foreach (GameObject item in items) {

			item.GetComponent<ItemController>().ResetItem();

		}

	}

}
