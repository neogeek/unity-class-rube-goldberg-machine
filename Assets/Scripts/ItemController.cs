﻿using UnityEngine;

public class ItemController : MonoBehaviour {

	private GameStart gameStart;

	private Vector3 originalPosition;
	private Quaternion originalRotation;

	public bool hasBeenActive = false;

	void Start () {

		gameStart = GameObject.Find("GameStart").gameObject.GetComponent<GameStart>();;

		originalPosition = gameObject.transform.position;
		originalRotation = gameObject.transform.rotation;

	}

	public void ResetItem () {

		gameObject.transform.position = originalPosition;
		gameObject.transform.rotation = originalRotation;

	}

	void OnDrawGizmosSelected () {

		if (gameObject.GetComponent<Renderer>()) {

			Vector3 maxBounds = gameObject.GetComponent<Renderer>().bounds.max;
			Vector3 extendsBounds = gameObject.GetComponent<Renderer>().bounds.extents;

			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(new Vector3(maxBounds.x - extendsBounds.x * 2, maxBounds.y, maxBounds.z - extendsBounds.z), 0.25f);

		}

	}

	void OnCollisionEnter (Collision other) {

		HandleCollision(other);

	}

	void OnCollisionExit (Collision other) {

		HandleCollision(other);

	}

	void HandleCollision (Collision other) {

		if (other.gameObject.tag == "Item") {

			if (!hasBeenActive && other.gameObject.name == gameStart.currentActiveItem.name) {

				Debug.Log(gameObject.name);

				gameStart.ChangeActiveItem(gameObject);

				hasBeenActive = true;

			}

		}

	}

}
